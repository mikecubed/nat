/*

nat - Network Address Translator
Copyright (C) 2007  Michael Henderson and Valentin Koch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <libnet.h>
#include <nids.h>
#include <pcap.h>

#include "nat.h"

extern int pipelink[NUMPIPES][2];

// verbosity global (set it non-zero for more output
int verbose = 1;

// interface for packet injection and interface name
static char *outface = NULL;
static char *if_name = NULL;

// pipes to communicate through
static int inpipe;
static int outpipe;

/** Oct macro
 * Used to help display IP address in dotted decimal notation
 *
 * \param x is an ip address (32bit int)
 * \param y is the octet (1 - 4)
 * 1 is leftmost octet (assuming host byte order)
 * 4 is rightmost octet
 */
#define Oct(x, y) (((x) >> (8*(4-(y)))) & 0xFF)

#include "listener.h"

// Listener private data
struct listener_str
{
	char* _if_in;
	char* _if_out;
};

// private function declarations
static void ip_func (struct ip *pkt, int len);
static int translate_datagram(struct ip* in, struct tuple4* change);
static void write_packet(struct ip*);

// ctor
Listener create_listener()
{
	Listener this;

	this = (Listener) malloc(sizeof(struct listener_str));

	return this;
}

// dtor
void delete_listener(Listener this)
{
	free(this);
}

// listener
void listen_iface(Listener this, char* if_in, char* if_out, void* child_listener, int lp_in, int lp_out, char* name)
{
	printf("Listening on interface: %s\n", if_in);
	this->_if_in = if_in;
	this->_if_out = if_out;

	// set the global variables
	outface = if_out;
	inpipe = lp_in;
	outpipe = lp_out;
	if_name = name;

	nids_params.device = if_in;
	nids_params.promisc = 1;	

	// initialize packet sniffing...
	if (!nids_init ()) {
		fprintf(stderr,"%s\n",nids_errbuf);
		exit(1);
	}

	nids_register_ip(ip_func);
	nids_run ();

}

/** ip_func (private)
 *  
 *  IP packet handler callback
 *  \param pkt  pointer to the packet
 *  \param len  useless
 */
static void ip_func (struct ip *pkt, int len) 
{
	/* only want tcp packets */
	if (pkt->ip_p != IPPROTO_TCP) {
		return;
	}

	//find the start of the tcp header
	struct tcphdr * tcpheader;
	char * buff = (char*)(pkt) + sizeof(struct ip);
	tcpheader = (struct tcphdr*)buff;

	if (verbose) {
			
		//printf("IP Packet received.\n");
		//printf("IP Version: %d header length: %d datagram length: %d ttl: %d tos: %d\n", 
		//pkt->ip_v, pkt->ip_hl*4, ntohs(pkt->ip_len), pkt->ip_ttl, pkt->ip_tos);
		//printf("id: %d header frag offset: %d protocol: %d\n", 
		//ntohs(pkt->ip_id), pkt->ip_off, pkt->ip_p);
	
		unsigned int saddr = ntohl(pkt->ip_src.s_addr);
		unsigned int daddr = ntohl(pkt->ip_dst.s_addr);
		
		if (pkt->ip_p == IPPROTO_TCP) {

			printf("%s: src addr: %d.%d.%d.%d:%d ",if_name, Oct(saddr, 1),
			       Oct(saddr, 2), Oct(saddr, 3), Oct(saddr, 4),
			       ntohs(tcpheader->source) );
			printf("dest addr: %d.%d.%d.%d:%d\n", Oct(daddr, 1),
			       Oct(daddr, 2), Oct(daddr, 3), Oct(daddr, 4),
			       ntohs(tcpheader->dest));

			//printf("TCP Packet\n");
			//printf("source port: %d dest port: %d\n",
			//       ntohs(tcpheader->source), ntohs(tcpheader->dest));
			//printf("seq: %u ack: %u\n",
			       //ntohl(tcpheader->seq),ntohl(tcpheader->ack_seq));
			       //printf("tcp header size: %d tcp window: %d \n",
			       //4*tcpheader->doff, ntohs(tcpheader->window));
			//buff += (4*tcpheader->doff);
			
			//printf("fin: %d syn: %d rst: %d psh: %d ack: %d urg: %d\n\n",
			       //tcpheader->fin,
			       //tcpheader->syn,
			       //tcpheader->rst,
			       //tcpheader->psh,
			       //tcpheader->ack,
			       //tcpheader->urg);
	
			// print the first 8 bytes of the payload
			//printf("%c%c%c%c%c%c%c%c\n\n",buff[0],buff[1],buff[2],buff[3],buff[4],buff[5],buff[6],buff[7]);
		}
	}
	
	// grab the connection details
	struct tuple4 in, out;
	in.source = tcpheader->source;
	in.dest = tcpheader->dest;
	in.saddr = pkt->ip_src.s_addr;
	in.daddr = pkt->ip_dst.s_addr;

	// now pipe it through
	write(pipelink[outpipe][WRITE],(void*)&in,sizeof(struct tuple4));
	
	// and read back the translated tuple4		
	int ret = read(pipelink[inpipe][READ],(void*)&out,sizeof(struct tuple4));
	struct in_addr src, dst;
	src.s_addr = out.saddr;
	char srcbuff[16];
	strcpy(srcbuff, inet_ntoa(src)); 
	dst.s_addr = out.daddr;
	
	// print the translated IPs and ports
	printf("%s: ret: %u, src port: %u, src addr: %s, dst port: %u, dst addr: %s \n\n",
		if_name, ret, ntohs(out.source), srcbuff, ntohs(out.dest), inet_ntoa(dst));
	
	if (ret <= 0 || out.source == 0 || out.dest == 0 ||
		out.saddr == 0 || out.daddr == 0) {
		fprintf(stderr, "%s: NAT translation failed\n", if_name);
		return;
	}

	// adjust the packet
	translate_datagram(pkt, &out);

	write_packet( pkt );
	return;
}


/** write_packet (private)
 *  
 *  writes a TCP packet onto the wire
 * \param pkt  pointer to an existing packet
 */
static void write_packet(struct ip* pkt)
{
	char errbuf[LIBNET_ERRBUF_SIZE];
	libnet_t *l;
	libnet_ptag_t t;
	int c;

	char * dev = outface;

	l = libnet_init(
            LIBNET_RAW4,		// injection type
            dev,			// network interface 
            errbuf);			// error buffer 

	if (l == NULL)
	{
		fprintf(stderr, "libnet_init() failed: %s", errbuf);
		return;
	}

	// find the offsets

	int pksize = ntohs(pkt->ip_len);
	int tcpoffset = pkt->ip_hl * 4;
	int tcpsize = pksize - tcpoffset;

	struct tcphdr * tcpstart = (struct tcphdr*)((char *)pkt + tcpoffset);

	int optionsize = (tcpstart->doff * 4) - LIBNET_TCP_H;
	char * optstart = (char*)tcpstart + LIBNET_TCP_H;  // TCP header is 20 bytes + options
	char * payload = (char*)tcpstart + (tcpstart->doff*4);
	int payloadsize = tcpsize - (tcpstart->doff * 4);

	// can't pass a non NULL pointer if there is no payload
	if (payloadsize == 0) {
		payload = NULL;
	}

	if (libnet_build_tcp_options(optstart,optionsize, l, 0) == -1) {
		fprintf(stderr, "Error: %s \n", libnet_geterror(l));
		libnet_destroy(l);
		return;
	}

	// build our control flag info 
	unsigned char cont = 	(tcpstart->fin * TH_FIN) | 
				(tcpstart->syn * TH_SYN) |
				(tcpstart->rst * TH_RST) |
				(tcpstart->psh * TH_PUSH)|
				(tcpstart->ack * TH_ACK) |
				(tcpstart->urg * TH_URG);

	// now build our new tcp header
	t = libnet_build_tcp(
		ntohs(tcpstart->source),			// source port 
		ntohs(tcpstart->dest),				// destination port 
		ntohl(tcpstart->seq),                           // sequence number 
		ntohl(tcpstart->ack_seq),                       // acknowledgement num 
		cont,                                     	// control flags 
		ntohs(tcpstart->window),                        // window size 
		0,                                          	// checksum 
		ntohs(tcpstart->urg_ptr),                       // urgent pointer 
		payloadsize + LIBNET_TCP_H + optionsize,        // TCP packet size 
		payload,                   			// payload 
		payloadsize,                                  	// payload size 
		l,                                          	// libnet handle 
		0);

	if (t == -1) {
		fprintf(stderr, "Error: %s \n", libnet_geterror(l));
		return;
	}

	t = libnet_build_ipv4(
		LIBNET_IPV4_H + LIBNET_TCP_H + payloadsize + optionsize,	// length 
		pkt->ip_tos,							// TOS 
		ntohs(pkt->ip_id),						// IP ID 
		htons(64),							// IP Frag 
		pkt->ip_ttl,							// TTL 
		IPPROTO_TCP,							// protocol 
		0,								// checksum 
		pkt->ip_src.s_addr,						// source IP 
		pkt->ip_dst.s_addr,						// destination IP 
		NULL,								// payload 
		0,								// payload size 
		l,								// libnet handle 
		0);								// libnet id 

	if (t == -1) {
		fprintf(stderr, "Error: %s \n", libnet_geterror(l));
		libnet_destroy(l);
		return;
	}


	c = libnet_write(l);
   	if (c == -1) {
        	fprintf(stderr, "Write error: %s\n", libnet_geterror(l));
   	} else {
		printf("%s: Wrote %d byte TCP packet; check the wire.\n\n", if_name, c);
   	}

	libnet_destroy(l);
	return;

}


/** translate_datagram (private)
 *
 * assumes the IP datagram contains a TCP segment
 * expects data to be in network byte order
 * \param in  pointer to a packet
 * \param change tuple4 struct containing requested changes
 * \return -1 on failure
 */
static int translate_datagram(struct ip* in, struct tuple4 *change)
{
	// check to see if we have a TCP segment
	if (in->ip_p != IPPROTO_TCP) {
		return -1;
	}

	// first we change the src/dest ip address in the IP header 
	// note: all data in struct up is in network byte order 
	in->ip_src.s_addr = change->saddr;
	in->ip_dst.s_addr = change->daddr;

	// find the tcp header offset in the ip datagram (in bytes)
	int offset = in->ip_hl * 4;
	struct tcphdr * tcp_seg = (struct tcphdr*)((unsigned char *)in + offset);

	// now we need to change the src/dest ports in the TCP header 
	tcp_seg->source = change->source;
	tcp_seg->dest = change->dest;

	return 0;
}

