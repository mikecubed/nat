DST_DIR = Release

SRC_DIR = src

INC_DIR = include

ifeq (,$(filter $(DST_DIR),$(notdir $(CURDIR))))

MAKETARGET = $(MAKE) --no-print-directory -C $@ -f $(CURDIR)/Makefile \
		SRCDIR=$(CURDIR)/$(SRC_DIR) INCDIR=$(CURDIR)/$(INC_DIR) \
		LIBDIR=$(CURDIR)/$(LIB_DIR) $(MAKECMDGOALS)

.PHONY: $(DST_DIR)
$(DST_DIR):
	+@[ -d $@ ] || mkdir -p $@
	+@$(MAKETARGET)

.PHONY: clean
clean:
	rm -rf $(DST_DIR)

else

CC	= gcc

DEBUG	= -ggdb

INCLUDE = -I$(INCDIR)

CCFLAGS = -MD $(INCLUDE) -O $(DEBUG)

LIBS	= -L$(LIBDIR) -lnids

VPATH	= $(SRCDIR):$(INCDIR):$(LIBDIR)

TARGET	= nat

SRCS	= $(shell find $(SRCDIR) -name *.c -printf "%f\n")

OBJS	= $(SRCS:.c=.o)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $@ $^ $(LIBS) -pthread

%.o : %.c
	$(CC) -c $(CCFLAGS) $< -o $@
	cp $*.d $*.dep; \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	-e '/^$$/ d' -e 's/$$/ :/' < $*.d >> $*.dep; \
	rm -f $*.d

-include $(OBJS:.o=.dep)

endif
