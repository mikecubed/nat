/* nat.h - Network Address Translation Router
 *
 * This is the NAT router application
 *
 * Copyright 2007 by Valentin Koch and Michael Henderson
 */
#ifndef __NAT_H
#define __NAT_H

#define READ 0
#define WRITE 1

#define TRIN 0
#define TROUT 1
#define UTIN 2
#define UTOUT 3 

#define NUMPIPES 4

#endif
