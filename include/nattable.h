/* nattable.h - Network Address Translation Table
 *
 * The nat table stores the information for all connections.
 *
 * Copyright 2007 by Valentin Koch and Michael Henderson
 */
#ifndef NATTABLE_H
#define NATTABLE_H


// for the tuple4 struct
#include <nids.h>

/** \class NATtable
 *
 *  structure containing the data for the NATtable
 */
typedef struct nattable_str* NATtable;

/** create_table  
 *
 *  NATTable constructor
 *  \param ext_ip  IP address to set as the external IP
 *  \return NATtable object or NULL
 */
NATtable create_table(unsigned int ext_ip);

/** delete_table  
 *
 *  NATTable destructor
 *
 *  WARNING:  You must call delete_table to destroy the object
 *            or you will leak memory.
 *  \param this the NATtable object to destroy
 */
void delete_table(NATtable this);

/** translate_trust  
 *
 *  Main function for the trusted nat translator 
 *
 *  \param this NATtable object 
 */
void* translate_trust(void* this);

/** translate_untrust  
 *
 *  Main function for the untrusted nat translator 
 *
 *  \param this NATtable object 
 */
void* translate_untrust(void* this);

#endif
